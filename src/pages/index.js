import React from 'react'
import { graphql } from 'gatsby'
import get from 'lodash/get'
import { Helmet } from 'react-helmet'
import Layout from '../components/layout'
import ContentBlock from '../components/content-block'

class RootIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const posts = get(this, 'props.data.allContentfulBlogPost.edges')
    const homepageContentBlock = get(this, 'props.data.allContentfulContentBlock.edges')[0]

    return (
      <Layout location={this.props.location}>
        <div style={{ background: '#fff' }}>
          <Helmet title={siteTitle} />
          <div>
            
            <ContentBlock content={homepageContentBlock.node}/>

          </div>
        </div>
      </Layout>
    )
  }
}

export default RootIndex

export const pageQuery = graphql`
  query HomeQuery {
    allContentfulContentBlock {
      edges {
        node {
          content {
            __typename
            ... on ContentfulBanner {
              name
              stretchImage
              blackAndWhiteBackground
              sidePadding
              whiteHeader
              height
              header
              gradientBackground
              darkOverlay
              backgroundImage {
                fixed(
                  width: 1400
                ) {
                  src
                }
              }
              items {
                __typename
                ... on ContentfulBannerItem {
                  body
                  header
                }
                ... on ContentfulImage {
                  name
                  hideOnMobile
                  image {
                    fluid(maxWidth: 600, resizingBehavior: SCALE) {
                      ...GatsbyContentfulFluid_tracedSVG
                    }
                  }
                  maxWidth
                  link
                }
              }
            }
            ... on ContentfulBannerStatement {
              name
              headerLine1
              headerLine2
              headerLine3
              bodyText
              stretchImage
              blackAndWhiteBackground
              height
              gradientBackground
              backgroundImage {
                fixed(
                  width: 1400
                ) {
                  src
                }
              }
            }
            ... on ContentfulSectionInformation {
              name
              title
              fullWidth
              gradientHeader
              smallHeader
              text {
                childMarkdownRemark {
                  html
                }
              }
            }
            ... on ContentfulHeader {
              title
              links {
                text
                name
              }
            }
            ... on ContentfulVideo {
              name
              link
            }
            ... on ContentfulPageAnchor {
              name
              tag
              topMargin
              mobileTopMargin
            }
            ... on ContentfulTeamImages {
              name
              set {
                name
                bio
                photo {
                  fluid(maxWidth: 350, maxHeight: 350, resizingBehavior: SCALE) {
                    ...GatsbyContentfulFluid_tracedSVG
                  }
                }
              }
            }
            ... on ContentfulSectionHeader {
              name
              description
              sectionTitle
            }
            ... on ContentfulArticlePreviews {
              id
              name
              blog {
                title
                slug
                website
                publishDate(formatString: "MMMM DD, YYYY")
                heroImage {
                  fluid(maxWidth: 350, maxHeight: 196, resizingBehavior: SCALE) {
                    ...GatsbyContentfulFluid_tracedSVG
                  }
                }
                description {
                  childMarkdownRemark {
                    html
                  }
                }
              }
            }
            ... on ContentfulSectionList {
              id
              name
              title
              subtitle
              greyBackground
            }
            ... on ContentfulTwinMacro {
              name
              greyBackground
              increaseSideMargin
              swapOrderForMobile
              leftMacro {
                __typename
                ... on ContentfulQuestionList {
                  questions {
                    answer
                    question
                    name
                  }
                }
                ... on ContentfulSectionInformation {
                  name
                  title
                  fullWidth
                  gradientHeader
                  smallHeader
                  text {
                    childMarkdownRemark {
                      html
                    }
                  }
                }
                ... on ContentfulBrainInfographic {
                  name
                }
                ... on ContentfulImage {
                  name
                  hideOnMobile
                  image {
                    fluid(maxWidth: 600, resizingBehavior: SCALE) {
                      ...GatsbyContentfulFluid_tracedSVG
                    }
                  }
                  maxWidth
                }
              }
              rightMacro {
                __typename
                ... on ContentfulImage {
                  name
                  hideOnMobile
                  image {
                    fluid(maxWidth: 600, resizingBehavior: SCALE) {
                      ...GatsbyContentfulFluid_tracedSVG
                    }
                  }
                  maxWidth
                }
                ... on ContentfulSectionInformation {
                  name
                  title
                  fullWidth
                  gradientHeader
                  smallHeader
                  text {
                    childMarkdownRemark {
                      html
                    }
                  }
                }
              }
              splitPosition
            }
          }
        }
      }
    }
  }
`
