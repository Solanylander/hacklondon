import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './footer.module.css'
import SectionInformation from './section-information'
import footer_png from "../images/footer_background.png"

export default ({ footer }) => (
<div class={styles.container + " " + styles.no_select}><a href="#team" title="Check out our team!">Check out our team!</a>
  <img class={styles.curve} src={footer_png} />
    <div class={styles.footer_info}>
        <div>
            <div class={styles.contact_info_area}>
                <div class={styles.contact_info}>
                    <a href="www.linkedin.com/company/third-eye-intel">LinkedIn</a>
                    <br />
                    <a href="mailto:contact@thirdeye.health">contact@thirdeye.health</a>
                </div>
            </div>
        </div>
    </div>
</div>
)
