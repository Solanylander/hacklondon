import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './article-preview.module.css'

export default ({ article }) => (
  <div className={`${styles.preview} recent-article`}> 
    <a href={article.slug} target="_blank"><Img alt="" fluid={article.heroImage.fluid} /></a>
    <div className={`${styles.postMeta} mt-20`}>
      <small className={styles.authorName}>{article.website}</small>
      <small className={styles.authorDate}>{article.publishDate}</small>
    </div>
    <h2 className={styles.previewTitle + " mt-20"}>
      <a href={article.slug} target="_blank">{article.title}</a>
    </h2>
    <div className={styles.previewDescription}
      dangerouslySetInnerHTML={{
        __html: article.description.childMarkdownRemark.html,
      }}
    />
    <a className={styles.readMoreButton} href={article.slug} target="_blank">Read More</a>
  </div>
)
