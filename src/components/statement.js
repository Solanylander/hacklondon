import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './statement.module.css'

export default ({ statement }) => (
<div class={styles.container + (statement.coloured ? " " + styles.coloured : "")}>
    <span class={styles.value}>{statement.value + " "}</span><span class={styles.context}>{statement.context}</span>
</div>
)
