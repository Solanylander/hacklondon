import React from 'react'
import { Link } from 'gatsby'
import { graphql } from 'gatsby'
import get from 'lodash/get'
import Img from 'gatsby-image'
import Header from '../components/header'
import Banner from '../components/banner'
import BannerStatement from '../components/banner-statement'
import TwinMacro from '../components/twin-macro'
import SectionHeader from '../components/section-header'
import SectionInformation from '../components/section-information'
import Video from '../components/video'
import MailingListSignUp from '../components/mailing-list-sign-up'
import ArticlePreviews from '../components/article-previews'
import ContactForm from '../components/contact-form'
import Footer from '../components/footer'
import QuestionList from '../components/question-list'
import Image from '../components/image'
import SectionList from '../components/section-list'
import Statement from '../components/statement'
import PageFlair from '../components/page-flair'
import Dropdown from '../components/dropdown'
import CardSet from '../components/card-set'
import TeamImages from '../components/team-images'
import PageAnchor from '../components/page-anchor'

import styles from './content-block.module.css'


export default ({ content }) => (

<div class={styles.container + " section-heading text-center"}>
    {content.content.map((element, index) => {
        switch(element.__typename){
            case "ContentfulHeader":
                return (
                    <div>
                        <Header headerInfo={element}/>
                    </div>
                )
            case "ContentfulBanner":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <Banner banner={element}/>
                    </div>
                )
            case "ContentfulDropdown":
                return (
                    <div>
                        <Dropdown dropdown={element} counter={index}/>
                    </div>
                )
            case "ContentfulPageAnchor":
                return (
                    <PageAnchor params={element}/>
                )
            case "ContentfulPageFlair":
                return (
                        <PageFlair pageFlair={element}/>
                )
            case "ContentfulBannerStatement":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <BannerStatement bannerStatement={element}/>
                    </div>
                )
            case "ContentfulCardSet":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <CardSet cardSet={element}/>
                    </div>
                )
            case "ContentfulTwinMacro":
                return (
                    <div>
                        <TwinMacro twinMacro={element}/>
                    </div>
                )
            case "ContentfulSectionInformation":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <SectionInformation sectionInformation={element}/>
                    </div>
                )
            case "ContentfulSectionHeader":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <SectionHeader sectionHeader={element}/>
                    </div>
                )
            case "ContentfulStatement":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <Statement statement={element}/>
                    </div>
                )
            case "ContentfulVideo":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <Video video={element}/>
                    </div>
                )
            case "ContentfulMailingListSignUp":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <MailingListSignUp mailingListSignUp=""/>
                    </div>
                )
            case "ContentfulSectionList":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <SectionList sectionList={element}/>
                    </div>
                )
            case "ContentfulArticlePreviews":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <ArticlePreviews articles={element}/>
                    </div>
                )
            case "ContentfulContactForm":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <ContactForm contactForm=""/>
                    </div>
                )
                
            case "ContentfulImage":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <Image image={element}/>
                    </div>
                )
            case "ContentfulTeamImages":
                return (
                    <div>
                        <TeamImages params={element}/>
                    </div>
                ) 
            case "ContentfulFooter":
                return (
                    <div>
                        <Footer footer=""/>
                    </div>
                )
            case "ContentfulQuestionList":
                return (
                    <div data-sal="slide-up" data-sal-duration="1000">
                        <QuestionList questionList={element}/>
                    </div>
                )
            default:
                return (
                    <h3>{element.__typename}</h3>
                )
        }
    })}
</div>
)

