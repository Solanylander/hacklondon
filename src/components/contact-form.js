import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './contact-form.module.css'
import SectionInformation from './section-information'

export default ({ contactForm }) => (

<div class={styles.form}>
    <form action="mailto:solanylander@gmail.com" method="post" id="main_contact_form" enctype="text/plain">
        <div>
            <div class="col-12">
                <div id="success_fail_info"></div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class={styles.group}>
                        <input type="text" name="name" id="name" required="" placeholder="Name" />
                        <span class={styles.highlight}></span>
                        <span class={styles.bar}></span>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class={styles.group}>
                        <input type="text" name="email" id="email" required="" placeholder="Email" />
                        <span class={styles.highlight}></span>
                        <span class={styles.bar}></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class={styles.group}>
                        <input type="text" name="subject" id="subject" required="" placeholder="Subject" />
                        <span class={styles.highlight}></span>
                        <span class={styles.bar}></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class={styles.group}>
                        <textarea name="message" id="message" required="" placeholder="Message" ></textarea>
                        <span class={styles.highlight}></span>
                        <span class={styles.bar}></span>
                    </div>
                </div>
            </div>
            <button className={styles.sendMessage}>Submit</button>
        </div>
    </form>    
</div>


)
