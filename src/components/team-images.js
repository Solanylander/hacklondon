import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './team-images.module.css'
import SectionInformation from '../components/section-information'

export default (params) => (
<div className={styles.container}>
  <div class={"d-flex justify-content-center " + styles.article_wrapper}>
    {params.params.set.map(( node ) => {
            return (
                <div class={styles.profile_wrapper}>
                    <div class={styles.profile_photo}>
                        <Img alt="" fluid={node.photo.fluid}/>
                    </div>
                    <div class={styles.name}>{node.name}</div>
                    <div class={styles.bio}>{node.bio}</div>
                </div>
            )
        })}
  </div>
</div>
)
