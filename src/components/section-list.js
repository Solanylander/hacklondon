import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './section-list.module.css'

export default ({ sectionList }) => (
<div class={styles.container + (sectionList.greyBackground ? " " + styles.grey_background : "")}>
    <h4 className={styles.title}>{sectionList.title}</h4>
    <h5 className={styles.subtitle}>{sectionList.subtitle}</h5>

    <ul className={styles.bulletPoints}>
        {sectionList.bulletPoints && sectionList.bulletPoints.map(element => {
            return <li className={styles.bulletPoint}>{element}</li>
        })}
    </ul>
</div>
)
