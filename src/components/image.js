import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './image.module.css'

export default ({ image, cap, left }) => (
  <div class={styles.container + (image.hideOnMobile ? " " + styles.hideOnMobile : "") + (left ? " " + styles.left_macro : "") + (cap ? " " + styles.capped_container : "")}  style={{ "max-width" : image.maxWidth + "px" }}>
      <Img alt="" fluid={image.image.fluid}/>
  </div>
)
