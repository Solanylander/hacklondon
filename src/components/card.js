import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './card.module.css'

export default ({ card, index }) => (
<div class={styles.container + " col-12 col-md-4 " + (index == 1 ? styles.drop : "")}>
    <div class={styles.inner_container}>
        <h6 class={styles.header}>{card.header}</h6>
        <Img class="image" alt="" fluid={card.image.fluid} />
        <p class={styles.body}>{card.body}</p>
    </div>
</div>
)
