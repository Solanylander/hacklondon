import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './mailing-list-sign-up.module.css'
import SectionInformation from './section-information'

import facebook_png from "../images/social_media/facebook.png"
import github_png from "../images/social_media/github.png"
import linked_in_png from "../images/social_media/linked_in.png"
import updates_png from "../images/polygons/updates.png"


export default ({ mailingListSignUp }) => (
<section class={"container " + styles.container} id="start">
    <div class={styles.subscribe}>
        <div class="row">
            <div class="col-sm-12">
                <div class={styles.subscribeWrapper}>

                    <div class={styles.sectionHeading + " " + styles.textCenter}>
                        <h2>Don’t Miss Our News And Updates!</h2>
                        <p>Subscribe to hear about our latest news.</p>
                    </div>
                    <div class={styles.serviceText}>

                        <div class="col-lg-8 col-lg-offset-2 col-md-8 offset-md-2 col-xs-12 text-center">
                            <div class={styles.group}>
                                <div class={styles.mailing_list_inputs + " row"}>
                                    <input class={styles.mailing_list_email} type="text" name="subject" required="" placeholder="enter your email" />
                                    {/* <button class={styles.submit_btn}/> */}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class={"col-md-2 offset-md-5 col-12 text-center " + styles.social_list_div}>
                                <a href="index.html" class={"button " + styles.email_button}>Submit</a>
                            </div>
                            <div class={styles.social_list_wrapper + " col-12 col-md-3"}>
                                <ul class={"list-unstyled list-inline " + styles.social_list}>
                                    
                                    {/* <li><a class="fa fa-linked-in-square" href="index.html"><img src={github_png} /></a></li>
                                    <li><a class="fa fa-github-square" href="index.html"><img src={linked_in_png} /></a></li> */}
                                </ul>
                            </div>
                        </div>

                        <img class={styles.updates_mesh} src={updates_png} />
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
)
