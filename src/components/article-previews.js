import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './article-previews.module.css'
import ArticlePreview from './article-preview'

export default ({ articles }) => (


<div className={styles.container}>
  <div class={"d-flex justify-content-center " + styles.article_wrapper}>
    <ul className={"article-list " + styles.articleList}>
      {articles.blog.map(( node ) => {
        return (
          <li key={node.slug}>
            <ArticlePreview article={node} />
          </li>
        )
      })}
    </ul>
  </div>
</div>
)
