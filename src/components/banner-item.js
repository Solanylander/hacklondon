import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './banner-item.module.css'
import SectionInformation from '../components/section-information'

export default ({ banner_item, index }) => (
<div class={styles.pointWrapper + " col-6 col-md-4 col-lg-2 " + (index == 0 ? "offset-lg-1" : "") + (index == 4 ? " offset-3 offset-md-0 " : "")}>
    <div data-sal="slide-right" data-sal-duration="2000" data-sal-delay={((index + 1) * 2) + "00"}>
        <h6 class={styles.header}>{banner_item.header}</h6>
        <p class={styles.body}>{banner_item.body}</p>
    </div>
</div>
)
