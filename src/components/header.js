import React, { useEffect } from 'react'
import styles from './header.module.css'
import third_eye_svg from "../images/logos/third_eye.svg"

import { Link } from 'gatsby'

let toggleHeaderMenu = () => {
  var navbar = document.getElementById('navbarText')
  
  if (navbar.classList.contains('collapse')){
    navbar.classList.remove('collapse')
  }
  else
  {
    navbar.classList.add('collapse')
  }
};

let navigatePage = (tag) => {
  document.querySelector('#page_anchor_' + tag).scrollIntoView({ 
    behavior: 'smooth' 
  });
};

export default (headerInfo) => {
  useEffect(() => {
    var scroll_position = window.scrollY
    var offset = 0


    if(window.attachEvent) {
      window.attachEvent('onresize', function() {
        if (window.innerWidth >= 992){
          var navbar = document.getElementById('navbarText')
      
          if (!navbar.classList.contains('collapse')){
            navbar.classList.add('collapse')
          }
        }
      });
    }
    else if(window.addEventListener) {
        window.addEventListener('resize', function() {
          if (window.innerWidth >= 992){
            var navbar = document.getElementById('navbarText')
        
            if (!navbar.classList.contains('collapse')){
              navbar.classList.add('collapse')
            }
          }
        }, true);
    }
    
    document.addEventListener('scroll', _ => {
      if( /Android|webOS|iPhone|iPad|Mac|Macintosh|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var change = window.scrollY - scroll_position
        offset -= change * 0.2
  
        if (offset > 0)
        {
          offset = 0
        }
  
        if (offset < -70)
        {
          offset = -70
        }
  
        document.getElementById('navbar').style.marginTop = offset + 'px'
  
        scroll_position = window.scrollY
        }
        else{
        if (offset != 0)
        {
          offset = 0
          document.getElementById('navbar').style.marginTop = offset + 'px'
        }
      }
    })
  })
  return (
<nav class={styles.navbar + " navbar fixed-top navbar-expand-lg navbar-light bg-light"} id="navbar">
  <img class={styles.navbarBrand + " navbar-brand justify-content-start"} id="navbar-brand" href="#" src={third_eye_svg}/>
  <button class="navbar-toggler" onClick={() => toggleHeaderMenu()}>
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-end" id="navbarText">
    <ul class={styles.navbarLinks + " navbar-nav mr-auto"} id="navbar-nav">
      <li class="nav-item active">
        <a class={"nav-link " + styles.anchor} onClick={() => navigatePage("home")}>HOME</a>
      </li>
      <li class="nav-item">
        <a class={"nav-link " + styles.anchor} onClick={() => navigatePage("product")}>PRODUCT</a>
      </li>
      <li class="nav-item">
        <a class={"nav-link " + styles.anchor} onClick={() => navigatePage("technology")}>TECHNOLOGY</a>
      </li>
      <li class="nav-item">
        <a class={"nav-link " + styles.anchor} onClick={() => navigatePage("about")}>ABOUT</a>
      </li>
      <li class="nav-item">
        <a class={"nav-link " + styles.anchor} onClick={() => navigatePage("contact")}>CONTACT</a>
      </li>
      <li class="nav-item">
        <a class={"nav-link " + styles.linked_in_ref} href="https://www.linkedin.com/company/third-eye-intel/" target="_blank">LinkedIn</a>
      </li>
    </ul>
  </div>
</nav>
)}