import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './question-list.module.css'
import Question from './question'

export default ({ questionList }) => (
<div class="col-12 col-lg-12 col-md-12">
    <div class={styles.dream_faq_area}>
        <div>
            <dl>
                {questionList.questions.map((element, index) => {
                    return (
                        <Question question={element} question_number={index}/>
                    )
                })}
            </dl>
        </div>
    </div>
</div>
)