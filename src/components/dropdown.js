import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import SectionInformation from '../components/section-information'
import Image from '../components/image'

import styles from './dropdown.module.css'

let toggleDropdown = (counter) => {
  var arrow = document.getElementById(counter + '-arrow')
  var wrapper = document.getElementById(counter + '-wrapper')
  
  if (arrow)
  {
    if (arrow.classList.contains(styles.flipped))
    {
      arrow.className = styles.arrow
      wrapper.className = "col-12 " + styles.hiddenWrapperHide
      setTimeout(() => {  wrapper.className = "col-12 " + styles.hiddenWrapperHide + " " + styles.heightZero }, 1000);
    }
    else
    {
      arrow.className = styles.arrow + ' ' + styles.flipped
      wrapper.className = "col-12 " + styles.hiddenWrapper
    }
  }
};

export default ({ dropdown, counter }) => (
<div class={styles.wrapper + " " + (dropdown.greyBackground ? " " + styles.grey_background : "")}>
  <div class={styles.container} data-sal="slide-up" data-sal-duration="1000">
    <div class="row align-items-top">

        <div class={"col-12"}>
          <div>{generateMacro(dropdown.visibleContent)}</div>
        </div>

        <div>
          <div class={styles.arrow} id={counter + '-arrow'} onClick={() => toggleDropdown(counter)}>🠗</div>
        
        </div>
        <div class={"col-12 " + styles.hiddenWrapperHide + " " + styles.heightZero} id={counter + '-wrapper'}>
          <div>{generateMacro(dropdown.hiddenContent)}</div>
        </div>
    </div>
  </div>
</div>
)

function generateMacro(element) {
    switch(element.__typename){
        case "ContentfulSectionInformation":
            return (
                <SectionInformation sectionInformation={element}/>
            )
        case "ContentfulImage":
            return (
                <Image image={element}/>
            )
    }
    return <div />
}