import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import Card from '../components/card'

import styles from './card-set.module.css'

export default ({ cardSet }) => (
<div class="container">
  <div class="row">
    {cardSet.cards && cardSet.cards.map((element, index) => {
      return (
        <Card card={element} index={index}/>
      )
    })}
  </div>
</div>
)
