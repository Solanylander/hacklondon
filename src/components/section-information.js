import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './section-information.module.css'

export default ({ sectionInformation }) => (
    <div class={styles.container + (sectionInformation.fullWidth ? " " + styles.fullWidth : "") + (sectionInformation.smallHeader ? " " + styles.small_header : "")}>
        <h4 className={styles.title + (sectionInformation.gradientHeader ? " " + styles.gradient_header : "")}>{sectionInformation.title}</h4>
        {generateLineBreak(sectionInformation)}
        <div className={styles.sectionText}>
            <div className={styles.previewDescription}
            dangerouslySetInnerHTML={{
                __html: sectionInformation.text.childMarkdownRemark.html,
            }} />
        </div>
    </div>
)

function generateLineBreak(sectionInformation) {
    if (sectionInformation.smallHeader){
      return(<div />)
    }
    else{
        return(<br />)
    }
  }
