import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './video.module.css'

export default ({ video }) => (


<div class={styles.container + " col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12"}>
    <div class={styles.border}>
        <div class={styles.welcome_thumb}>
            <iframe src={video.link}> </iframe>
        </div>
    </div>
</div>
)
