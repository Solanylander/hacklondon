import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './page-flair.module.css'
import mesh_svg from "../images/outline_traingles.svg"

export default (pageFlair) => (
<div class={styles.container + (pageFlair.pageFlair.zIndexForward ? (" " + styles.forward) : "")}>
    <div class={styles.absolute_container}>
        <img style={{ "margin-top" : pageFlair.pageFlair.topMargin + "px" }} src={mesh_svg} />
    </div>
</div>
)
