import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './question.module.css'


let click = (selected_answer_id) => {
    var answers = document.getElementsByClassName(styles.answer);
    
    for (let i = 0; i < answers.length; i++) {
        if (answers[i].id == selected_answer_id) 
        {
            answers[i].className = styles.answer + " " + styles.open
        }
        else
        {
            answers[i].className = styles.answer + " " + styles.close
        }
    }
  };

export default ({ question, question_number }) => (
<div>
    <dt className={styles.question} onClick={() => click(("answer_" + question_number))}>{question.question}</dt>
    <dd className={styles.answer + " " + (question_number == 0 ? styles.open : styles.close)} id={"answer_" + question_number}>
        <p>{question.answer}</p>
    </dd>
</div>
)
