import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './brain-infographic.module.css'

import facebook_png from "../images/social_media/facebook.png"
import github_png from "../images/social_media/github.png"
import linked_in_png from "../images/social_media/linked_in.png"

import purple_brain_svg from "../images/infographic/purple_brain.svg"

import vitals_svg from "../images/infographic/vitals.svg"
import medication_svg from "../images/infographic/medication.svg"
import microscope_svg from "../images/infographic/microscope.svg"
import records_svg from "../images/infographic/records.svg"
import scans_svg from "../images/infographic/scans.svg"
import trace_svg from "../images/infographic/trace.svg"
import mesh_svg from "../images/infographic/mesh.svg"
import { range } from 'lodash'

const Mesh = (props) => {
  return <svg class={styles.mesh} viewBox="0 0 443.18 372.78">
      <g id="Layer_2">
          <g id="Layer_3">
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="229.63 243.26 223.92 196.2 217.1 140 306.82 205.34 265.47 225.66 229.63 243.26"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="229.63 243.26 265.47 225.66 306.82 205.34 284.47 225.66 258.4 249.35 229.63 243.26"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="258.4 249.35 284.47 225.66 306.82 205.34 313.25 210.39 321.04 216.51 295.53 229.89 258.4 249.35"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="321.04 216.51 313.25 210.39 306.82 205.34 321.61 186.73 338.31 165.73 321.04 216.51"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="258.4 249.35 295.53 229.89 321.04 216.51 326.88 225.66 334.24 237.17 308.91 241.24 258.4 249.35"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="334.24 237.17 326.88 225.66 321.04 216.51 338.85 215.96 356.62 215.41 348.25 223.55 334.24 237.17"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="321.04 216.51 338.85 215.96 356.62 215.41 338.31 165.73 321.04 216.51"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="258.4 249.35 308.91 241.24 334.24 237.17 323.94 269.59 311.22 309.62 285.58 280.36 258.4 249.35"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="311.22 309.62 323.94 269.59 334.24 237.17 340.75 267.94 346.77 296.41 311.22 309.62"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="334.24 237.17 340.75 267.94 346.77 296.41 398.38 249.35 370.05 243.97 334.24 237.17"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="334.24 237.17 370.05 243.97 398.38 249.35 375.55 224.64 334.24 237.17"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="229.63 243.26 223.92 196.2 217.1 140 191.86 181.58 175.79 208.05 201.01 224.54 229.63 243.26"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="175.79 208.05 191.86 181.58 217.1 140 217.1 140 188.27 164.8 157.08 191.63 175.79 208.05"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="217.1 140 188.27 164.8 157.08 191.63 176.92 156.96 202.71 111.9 217.1 140"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="157.08 191.63 176.92 156.96 202.71 111.9 149.9 130.1 87.06 151.76 157.08 191.63"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="175.79 208.05 201.01 224.54 229.63 243.26 152.09 239.2 166.22 220.63 175.79 208.05"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="152.09 239.2 166.22 220.63 175.79 208.05 157.08 191.63 154.46 216.6 152.09 239.2"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="152.09 239.2 154.46 216.6 157.08 191.63 133.95 205.34 112.82 217.87 133.89 229.31 152.09 239.2"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="112.82 217.87 133.95 205.34 157.08 191.63 119.04 187.48 81 183.33 98.91 202.77 112.82 217.87"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="157.08 191.63 119.04 187.48 81 183.33 87.06 151.76 157.08 191.63"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="112.82 217.87 133.89 229.31 152.09 239.2 134 269.14 110.87 307.42 112.82 217.87"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="110.87 307.42 134 269.14 152.09 239.2 165.54 276.14 176.92 307.42 110.87 307.42"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="152.09 239.2 165.54 276.14 176.92 307.42 204.29 274.1 229.63 243.26 152.09 239.2"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="176.92 307.42 204.29 274.1 229.63 243.26 218.19 288.64 209.31 323.84 176.92 307.42"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="209.31 323.84 218.19 288.64 229.63 243.26 234.81 265.69 240.12 288.63 209.31 323.84"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="240.12 288.63 234.81 265.69 229.63 243.26 258.4 249.35 240.12 288.63"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="258.4 249.35 271.7 287.05 281.09 313.68 311.22 309.62 285.58 280.36 258.4 249.35"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="281.09 313.68 271.7 287.05 258.4 249.35 240.12 288.63 281.09 313.68"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="338.31 165.73 287.44 138.22 238.77 111.9 271.68 157.09 306.82 205.34 321.61 186.73 338.31 165.73"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="238.77 111.9 287.44 138.22 338.31 165.73 278.68 95.98 238.77 111.9"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="306.82 205.34 271.68 157.09 238.77 111.9 228.98 124.59 217.1 140 306.82 205.34"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="217.1 140 228.98 124.59 238.77 111.9 202.71 111.9 217.1 140"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="81 183.33 98.91 202.77 112.82 217.87 78.63 221.59 81 183.33"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="87.06 151.76 168.35 57.39 189.3 90.62 202.71 111.9 149.9 130.1 87.06 151.76"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="202.71 111.9 189.3 90.62 168.35 57.39 211.34 47.57 207.03 79.73 202.71 111.9"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="202.71 111.9 207.03 79.73 211.34 47.57 238.77 111.9 202.71 111.9"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="334.24 237.17 375.55 224.64 356.62 215.41 348.25 223.55 334.24 237.17"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="211.34 47.57 261.96 69.98 238.77 111.9 211.34 47.57"/>
            <polygon class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="278.68 95.98 261.96 69.98 238.77 111.9 278.68 95.98"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="278.68 95.98 376.66 27.07 338.31 165.73 443.17 174.84"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="281.09 313.68 328.12 372.24 311.22 309.62"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="176.92 307.42 87.06 364.24 110.87 307.42"/>
            <line class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} x1="442.27" y1="174.1" x2="356.62" y2="215.41"/>
            <polyline class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} points="168.35 57.39 110.87 0.26 87.06 151.76 0.71 183.33 81 183.33"/>
            <line class={`mesh ` + styles.cls_1 + ` ` + styles.width_1} x1="78.63" y1="221.59" x2="0.71" y2="183.33"/>
            </g>
            </g>
          </svg> 
}

const updateSvgLines = () => setInterval(()=> {
  var elements = document.getElementsByClassName("mesh")
  var mesh_styles = [styles.cls_0, styles.cls_1, styles.cls_2, styles.cls_3, styles.cls_4, styles.cls_5, styles.cls_6, styles.cls_7]
  var width_styles = [styles.width_0, styles.width_1, styles.width_2]
  if (elements.length > 0){
    var index = 0
    while (index < elements.length) {
        elements[index].setAttribute('class', (width_styles[Math.floor(Math.random() * 3)] + ' mesh ' + mesh_styles[Math.floor(Math.random() * 8)]));
      index++;
    }
  }
}, 2000);

export default () => (
   
<div class={styles.container}>
  <div class={styles.brain_wrapper}>
    <img class={styles.purple_brain + ` ` + styles.info_button_brain} src={purple_brain_svg} />
  </div>

  <Mesh />
  <img class={styles.vitals + ` ` + styles.info_button} src={vitals_svg} />
  <img class={styles.scans + ` ` + styles.info_button} src={scans_svg} />
  <img class={styles.trace + ` ` + styles.info_button} src={trace_svg} />
  <img class={styles.medication + ` ` + styles.info_button} src={medication_svg} />
  <img class={styles.microscope + ` ` + styles.info_button} src={microscope_svg} />
  <img class={styles.records + ` ` + styles.info_button} src={records_svg} />
</div>

)
updateSvgLines();