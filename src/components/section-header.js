import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './section-header.module.css'

export default ({ sectionHeader }) => (
<div class={styles.container + " text-center " + (sectionHeader.greyBackground ? " " + styles.grey_background : "") + (sectionHeader.extraPadding ? " " + styles.extra_padding : "")}>
    <h2>{sectionHeader.sectionTitle}</h2>
    <p class={styles.description}>{sectionHeader.description}</p>
</div>
)
