import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './twin-macro.module.css'
import SectionInformation from '../components/section-information'
import BannerItem from '../components/banner-item'
import QuestionList from '../components/question-list'
import BrainInfographic from '../components/brain-infographic'
import Image from '../components/image'
import CardSet from '../components/card-set'
import SectionList from '../components/section-list'

export default ({ twinMacro }) => (
<div class={styles.wrapper + (twinMacro.greyBackground ? " " + styles.grey_background : "")}>
  <div class={styles.container + (twinMacro.fullWidth ? "" : " container") + (twinMacro.increaseSideMargin ? " " + styles.small_container : "")} data-sal="slide-up" data-sal-duration="1000">
    {generateMacroPair(twinMacro)}
  </div>
</div>
)

function generateMacroPair(twinMacro) {
  var orderingClassesFirstMacro = twinMacro.swapOrderForMobile ? 'order-2 order-lg-1' : ''
  var orderingClassesSecondMacro = twinMacro.swapOrderForMobile ? 'order-1 order-lg-2' : ''

  if (twinMacro.increaseSideMargin){
    return(
    <div class="row d-flex align-items-center">
      <div class={styles.small_macro + " col-12 " + (twinMacro.fullWidth ? "" : "col-lg-5 offset-lg-1 " + orderingClassesFirstMacro)}>
        <div>{generateMacro(twinMacro.leftMacro, true, true)}</div>
      </div>
      <div class={styles.small_macro + " col-12 " + (twinMacro.fullWidth ? "" : "col-lg-5 " + orderingClassesSecondMacro)}>
        <div>{generateMacro(twinMacro.rightMacro, true, false)}</div>
      </div>
    </div>)
  }
  else{
    return(
    <div class="row d-flex align-items-center">
      <div class={styles.macro + " col-12 " + (twinMacro.fullWidth ? "" : "col-lg-6 " + orderingClassesFirstMacro)}>
        <div>{generateMacro(twinMacro.leftMacro, false)}</div>
      </div>
      <div class={styles.macro + " col-12 " + (twinMacro.fullWidth ? "" : "col-lg-6 " + orderingClassesSecondMacro)}>
        <div>{generateMacro(twinMacro.rightMacro, false)}</div>
      </div>
    </div>)
  }
}

function generateMacro(element, cap, left) {
  if (element == null){
    return <div />
  }
  switch(element.__typename){
    case "ContentfulSectionInformation":
        return (
            <SectionInformation sectionInformation={element}/>
        )
    case "ContentfulQuestionList":
      return (
          <QuestionList questionList={element}/>
      )
    case "ContentfulImage":
        return (
            <Image image={element} cap={cap} left={left} />
        )
    case "ContentfulBrainInfographic":
      return (
          <BrainInfographic details={element}/>
      )
    case "ContentfulSectionList":
        return (
          <SectionList sectionList={element}/>
        )
    case "ContentfulCardSet":
      return (
        <CardSet cardSet={element}/>
      )
  }
}