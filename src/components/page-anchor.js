import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './page-anchor.module.css'

export default ({ params }) => (
<div class={styles.container}>
    <div class={styles.anchor + " " + (params.mobileTopMargin ? styles.mobile_top : "")} id={"page_anchor_" + params.tag}  style={{'top': params.topMargin + "px"}} />
</div>
)
