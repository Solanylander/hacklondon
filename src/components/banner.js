import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './banner.module.css'
import BannerItem from '../components/banner-item'
import Image from '../components/image'

export default ({ banner }) => (
   
<div class={styles.container + (banner.sidePadding ? " " + styles.t_container : '') + (banner.stretchImage ? (" " + styles.stretchImage) : "") + (banner.gradientBackground ? " " + (banner.blackAndWhiteBackground ? styles.bwGradientBackground : styles.gradientBackground) : "")}
  style={{
        'height': banner.height + "px", 
        'background-image': (banner.backgroundImage ? 'url(' + banner.backgroundImage.fixed.src + ')' : ''),
        'filter': (banner.blackAndWhiteBackground ? 'grayscale(100%)' : '')
  }}>
  <div class={(banner.sidePadding ? styles.topic_container : '') + " " + (banner.darkOverlay ? styles.dark_overlay : '')}>
    <div class={styles.header + (banner.whiteHeader ? (" " + styles.whiteText) : "")}  data-sal={(banner.sidePadding?"slide-up":"slide-right")} data-sal-duration="2000"  data-sal-delay="200">{banner.header}</div>
    <div class={"row " + styles.banner_row}>
      {banner.items && banner.items.map((element, index) => {
        switch(element.__typename){
          case "ContentfulBannerItem":
              return (
                <BannerItem banner_item={element} index={index}/>
              )
          case "ContentfulImage":
            if(index % 5 == 0 && index != 0)
            {
              return (
                <div class={" col-6 col-md-4 col-lg-2"}>
                  <Image image={element}/>
                </div>
              )
            }
            else
            {
              return (
                <div class={styles.image_element + " col-6 col-md-4 col-lg-2"}>
                  <Image image={element}/>
                </div>
              )
            }
        }
      })}
    </div>
  </div>
</div>

)