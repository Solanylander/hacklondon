import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './banner-statement.module.css'
import SectionInformation from '../components/section-information'

export default ({ bannerStatement }) => (

    
<div class={styles.container + (bannerStatement.stretchImage ? (" " + styles.stretchImage) : "") + (bannerStatement.gradientBackground ? " " + (bannerStatement.blackAndWhiteBackground ? styles.bwGradientBackground : styles.gradientBackground) : "")}
  style={{ 
        'background-image': (bannerStatement.backgroundImage ? 'url(' + bannerStatement.backgroundImage.fixed.src + ')' : ''),
        'filter': (bannerStatement.blackAndWhiteBackground ? 'grayscale(100%)' : '')
  }}>
  <div>
    <div>
        <span class={styles.title_statement} data-sal="slide-right" data-sal-duration="2000" data-sal-delay="600">
          {bannerStatement.headerLine1 != '' ? bannerStatement.headerLine1 : ""}
        </span>
        <br />
        <h3 data-sal="slide-right" data-sal-duration="2000" data-sal-delay="600">
          {bannerStatement.headerLine2 != '' ? bannerStatement.headerLine2 : ""}
          <br />
          {bannerStatement.headerLine3 != '' ? bannerStatement.headerLine3 : ""}
        </h3>
    </div>
  </div>
  <div>
    <div>
        <h5 data-sal="slide-right" data-sal-duration="2000" data-sal-delay="600">{bannerStatement.bodyText}</h5>
    </div>
  </div>
  <div class={styles.vertical_gradient} />
</div>

)
